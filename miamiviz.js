/**
MIAMIviz - Visualization for Mode of Action analysis
    Copyright (C) 2019 Christian-Alexander Dudek

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
const params = new URLSearchParams(window.location.search)
const WEBVERSION = !params.has('Qt')
const DEBUG = params.has('debug')
const VERSION = '1.1.1'

// Default UI variables
let name = 0 // Node name
let quantCutoff = 0
let distanceCutoff = 0.15
let variabilityCutoff = 0.05
let showSingleNodes = false
let showTargets = false
let showArrows = false
let showReferences = true
let nodesOverEdges = true
let removeEdges = false

// Style variables
const connectorRadius = 3
const nodeGap = 10
const pvalues = [0.05, 0.005]
const referenceColor = '#666666'
const lock_icon = '@' // '🔒'
// Custom plot colors
// let cols = ['#cc0000', '#00cc00', '#0000cc', '#cc00cc',
//             '#cccc00', '#00cccc', '#cccccc', '#000000']

// Global variables
let data, experiments, nodes, edges, config, targets, filename, color, sim, channel

// Javascript => Qt communication channel
if(!WEBVERSION) {
  channel = new QWebChannel(qt.webChannelTransport, function(c) {
    window.miami = c.objects.miamiapp
//    miami.ping('Javascript conneceted to frontend!')
  })
}

d3.select('#version').html(VERSION)

// Cross platform width/height determination
let width = window.innerWidth ||
            document.documentElement.clientWidth ||
            document.body.clientWidth
let height = window.innerHeight ||
            document.documentElement.clientHeight ||
            document.body.clientHeight

// Disable context menu
if (WEBVERSION && !DEBUG) {
    d3.select('body, body > *').on('contextmenu', () => {
      d3.event.preventDefault()
    })
}

// Resize handler
window.addEventListener('resize', () => {
    let old_width = width
    let old_height = height

    width = window.innerWidth ||
            document.documentElement.clientWidth ||
            document.body.clientWidth

    height = window.innerHeight ||
             document.documentElement.clientHeight ||
             document.body.clientHeight
    svg.attr('viewbox', `0 0 ${(width-2)} ${(height-2)}`)
        .attr('width', width-2)
        .attr('height', height-2)

    let dy = height - old_height
    let dx = width - old_width

    zoom.translateBy(svg, dx/2, dy/2)
})

d3.select(window)
    .on('mousemove', () => {
        if (tooltip.style('opacity') == 1) {
            let x = d3.event.pageX - Number(tooltip.style('width').slice(0,-2))/2
            let y = d3.event.pageY - Number(tooltip.style('height').slice(0,-2))-10
            tooltip.style('left', `${x}px`)
                .style('top', `${y}px`)
        }
    })

let zoom = d3.zoom().scaleExtent([.25, 8]).on('zoom', () => {
        graph.attr('transform', d3.event.transform)
    })

let tooltip = d3.select('body').append('div')
    .attr('class', 'tooltip')
    .style('opacity', '0')

let svg = d3.select('svg')
    .attr('viewbox', `0 0 ${(width-2)} ${(height-2)}`)
    .attr('width', width-2)
    .attr('height', height-2)
    .attr('xmlns', 'http://www.w3.org/2000/svg')
    .attr('version', '2.0')
    .attr('baseProfile', 'full')
    .style('pointer-events', 'all')
    .call(zoom)
    .on('dblclick.zoom', null)

svg.append('defs')

svg.append('defs')
    .append('marker')
    .attr('id', `circle_ref`)
    .attr('orient', 'auto')
    .attr('class', 'circle')
    .attr('markerWidth', connectorRadius)
    .attr('markerHeight', connectorRadius)
    .attr('markerUnits', 'userSpaceOnUse')
    .attr('overflow', 'visible')
    .append('circle')
    .attr('cx', 0)
    .attr('cy', 0)
    .attr('r', connectorRadius)
    .attr('fill', referenceColor)
    .attr('stroke', 'none')

let graph = svg.append('g').attr('id', 'graph')

graph.append('g').attr('id', 'nodeGroup')
graph.append('g').attr('id', 'edgeGroup')
graph.append('g').attr('id', 'targetGroup')

zoom.translateTo(svg, 0, 0)


/**
 * @function update_data
 * @brief Updates the data
 */
function update_data() {
    // Update experiments
    experiments.forEach(e => {})
    // Update edges
    edges.forEach(e => {
        if (e.experiment == 'Reference') {
            e.visible = config.miami_show_references
        } else {
            // Check if node can be removed
            e.removed = config.miami_remove_edges && find_path(e.source, e.target, true, 'Reference')[0].length > 1

            e.visible = !e.target.hidden && !e.source.hidden && e.distance <= config.miami_distance_cutoff &&
                     experiments.filter(x => x.name == e.experiment)[0].visible && !e.removed &&
                     e.target.quant.some(q => q.mean > config.miami_quant_cutoff) && e.source.quant.some(q => q.mean > config.miami_quant_cutoff)
        }
    })
    // Update nodes
    nodes.forEach(n => {
        if (n.is_ref) {
            n.visible = config.miami_show_references
        } else {
            n.visible = !n.hidden && n.quant.some(q => q.mean > config.miami_quant_cutoff ) && (config.miami_single_nodes || get_neighbours(n).length > 0)
        }
    })
}

/**
 * @function update
 * @brief Performs a complete update of data, graph and UI
 */
function update(refresh=true) {
    update_data()
    update_ui()
    update_nodes()
    update_edges()
    update_targets()

    if (config.miami_nodes_over_edges) {
        d3.select('#nodeGroup').raise()
    } else {
        d3.select('#edgeGroup').raise()
    }

    update_defs()

    // Update and restart the simulation.
    sim.nodes(nodes)
    sim.force('link').links(edges.filter(e => e.visible))
    if (refresh) {
        sim.alpha(.9).restart()
    }

}

/**
 * @function update_ui
 * @brief Updates the UI
 */
function update_ui() {
   // Update panels
    d3.select('#panels').selectAll('.panel')
        .data(nodes.filter(n => (n.visible || config.miami_single_nodes) && !n.hidden && n.showpanel))
        .join('div')
            // enter => enter.append('div')
                .attr('id', m => `panel-${m.id}`)
                .attr('class', 'panel')
                .html(d3.select('#panel').html())
                .style('left', m => {
                    if (m.px === undefined) {
                        let n = d3.select('#panels').selectAll('.panel').size()
                        m.px = 5 + n * 10
                    }
                    return `${m.px}px`
                })
                .style('top', m => {
                    if (m.py === undefined) {
                        let n = d3.select('#panels').selectAll('.panel').size()
                        let offset = d3.select('#ui').style('display') == 'block' ? 26 : 0
                        m.py = offset + n * 10
                    }
                    return `${m.py}px`
                })
                .call(
                    d3.drag()
                        .on('start', m => {
                            d3.select(`#panel-${m.id}`).raise()
                            d3.select(`#panel-${m.id}`)
                                .attr('data-x', d3.event.sourceEvent.screenX)
                                .attr('data-y', d3.event.sourceEvent.screenY)
                        })
                        .on('drag', m => {
                            let p = d3.select(`#panel-${m.id}`)
                            let dx = d3.event.sourceEvent.screenX - parseInt(p.attr('data-x'))
                            let dy = d3.event.sourceEvent.screenY - parseInt(p.attr('data-y'))
                            m.px = m.px ? m.px + dx : dx;
                            m.py = m.py ? m.py + dy : dy;
                            p.style('left', `${m.px}px`)
                                .style('top', `${m.py}px`)
                                .attr('data-x', d3.event.sourceEvent.screenX)
                                .attr('data-y', d3.event.sourceEvent.screenY)
                        })
                        .on('end', m => {
                            d3.select(`#panel-${m.id}`)
                                .attr('data-x', null)
                                .attr('data-y', null)
                        }))
                .call(m => {
                    m.select('.close').on('click', m => {
                        m.px = 10
                        m.py = 31
                        m.showpanel = false
                        update_ui()
                    })
                    m.style('left', m => `${m.px}px`)
                    m.style('top', m => `${m.py}px`)
                    m.select('.name').text(m => m.name)
                    m.select('.ion').text(m => m.ion)
                    m.select('.rt').text(m => `${(m.rt/1000/60).toFixed(2)} min`)
                    m.select('.ri').text(m => m.ri)
                    // m.select('.keggid').text(m => m.kegg_id)
                    m.select('.structure').html('')

                    m.select('.structure').append('img').attr('src', m => {
                        if (m.kegg_id != '') {
                            let kid = m.kegg_id.split(',')[0]
                            return `https://www.kegg.jp/Fig/compound/${kid}.gif`
                        }
                    })
                    m.select('.kegg_container').style('display', m => m.kegg_id == '' ? 'none' : 'block')
                    m.select('.keggid').html(m => `KEGG id: <a href="https://www.kegg.jp/dbget-bin/www_bget?cpd:${m.kegg_id}">${m.kegg_id}</a>`)

                    m.select('.library').text(m => m.library.split('/')[m.library.split('/').length-1])
                    m.select('.score').text(m => m.score.toFixed(2))
                    m.select('.mids').append(m => {
                        let e1 = experiments.filter(e => e.e1)[0].name
                        let e2 = experiments.filter(e => e.e2)[0].name
                        let v = m.variability.filter(d => d.e1 === e1 && d.e2 == e2)
                        return barplot(m.mids, v)
                    })
                    m.select('.quant').append(m => barplot(m.quant))
                    m.select('.middownload').on('click', m => {
                        let p = d3.select(`#panel-${m.id}`)
                        let payload = p.select('.mids').html()
                        p.select('.middownload')
                            .attr('href', m => {
                                return `data:application/octet-stream;base64,`+
                                       `${btoa(payload)}`
                                }
                            )
                            .attr('download', m => `miami_mids_node${m.id}.svg`)
                    })
                    m.select('.quantdownload').on('click', m => {
                        let p = d3.select(`#panel-${m.id}`)
                        let payload = p.select('.quant').html()
                        p.select('.quantdownload')
                            .attr('href', m => {
                                return `data:application/octet-stream;base64,`+
                                       `${btoa(payload)}`
                                }
                            )
                            .attr('download', m => `miami_mids_node${m.id}.svg`)
                    })
                    m.selectAll('.tab').on('click', (x, i, l) => {
                        m.selectAll('.tab').classed('shown', false)

                        m.select('.mid_tab').style('display', 'none')
                        m.select('.quant_tab').style('display', 'none')

                        d3.select(l[i]).classed('shown', true)
                        let t = d3.select(l[i]).attr('data-target')
                        m.select(`.${t}`).style('display', 'block')
                    })
                })
    if (!WEBVERSION) return
    // Update lists
    d3.select('#visibleNodes').selectAll('li')
        .data(nodes.filter(n => n.visible  || n.hidden).sort((a, b) => {
            return d3.descending(a.hidden ? 0 : 1, b.hidden ? 0 : 1) || d3.ascending(a.name, b.name)
        }))
        .join('li')
        .merge(d3.select('#visibleNodes').selectAll('li'))
        .style('opacity', n => n.hidden ? .5 : 1)
        .text(n => n.name)
        .style('list-style-type', n => n.showpanel ? 'square' : 'none')
        .attr('title', n => n.hidden ? 'Click to show again' : 'Click to open')
        .style('cursor', 'pointer')
        .on('click', n => {
            if (n.hidden) {
                n.hidden = false
                update()
            } else if (n.showpanel) {
                d3.select(`#panel-${n.id}`).raise()
            } else {
                d3.select(`#node-${n.id}`).dispatch('click')
            }
        })

    d3.select('#invisibleNodes').selectAll('li')
        .data(nodes.filter(n => !n.visible && !n.hidden))
        .join('li')
        .merge(d3.select('#invisibleNodes').selectAll('li'))
        .text(n => n.name)

    d3.select('#experimentsLabel').text(`Experiments (${experiments.filter(e => e.visible).length})`)
    d3.select('#invisibleNodeLabel').text(`Not shown (${nodes.filter(n => !n.visible && !n.hidden).length})`)
    d3.select('#visibleNodeLabel').text(`Metabolites (${nodes.filter(n => n.visible  || n.hidden).length})`)

    d3.select('#experiments').selectAll('li')
        .data(experiments)
        .join('li')
        .text(e => e.name)
        .attr('title', e => e.visible ? 'Click to hide' : 'Click to show')
        .style('color', e => color(e.name))
        .style('opacity', e => e.visible ? 1.0 : 0.33)
        .on('click', experiment_click)


    d3.selectAll('.variability_dropdown').selectAll('option')
        .data(experiments)
        .join('option')
        .text(e => e.name)
        .style('color', e => color(e.name))
        .attr('selected', e => e.e1)

    if (experiments.length >= 2) {
        d3.select('#experiment1').property('value', () => experiments.filter(e => e.e1)[0].name)
        d3.select('#experiment2').property('value', () => experiments.filter(e => e.e2)[0].name)
    }

    d3.selectAll('.variability_dropdown')
        .on('change',() => {
            let e1 = d3.select('#experiment1').property('value')
            let e2 = d3.select('#experiment2').property('value')

            select_experiments(e1, e2)
        })


    d3.selectAll('.pathway_dropdown').selectAll('option')
        .data(nodes.filter(n => n.visible && !n.hidden))
        .join('option')
        .text(e => e.name)
        .attr('value', e => e.id)

    d3.selectAll('.pathway_dropdown')
        .on('change',() => {
            let m1 = d3.select('#metabolite1').property('value')
            let m2 = d3.select('#metabolite2').property('value')

            highlight_path(m1, m2)
        })

    d3.select('#menu').raise()
}

function highlight_path(m1, m2) {

    d3.selectAll('.node').style('opacity', 1).select('rect')
        .classed('start', false).classed('end', false)
    d3.selectAll('.edge').style('opacity', 1).classed('path', false)
    d3.selectAll('.target').style('opacity', 1)

    if (m1 === m2 || m1 == -1 || m2 == -1) return

    d3.selectAll('.node').style('opacity', .25)
    d3.selectAll('.edge').style('opacity', .25)
    d3.selectAll('.target').style('opacity', .25)

    d3.selectAll(`#node-${m1}`).select('rect').classed('start', true)
    d3.selectAll(`#node-${m2}`).select('rect').classed('end', true)

    let p = find_path(d3.selectAll(`#node-${m1}`).datum(),
                      d3.selectAll(`#node-${m2}`).datum(),
                      true)[0]

    d3.selectAll('.node').filter(n => p.indexOf(n.id) > -1).style('opacity', 1)
    d3.selectAll('.edge').filter(e => p.indexOf(e.target.id) > -1 && p.indexOf(e.source.id)  > -1 ).style('opacity', 1)
        .classed('path', true)
}

/**
 * @function experiment_click
 * @brief Handles click on experiment legend
 */
function experiment_click(experiment) {
    if (typeof experiment == 'string') {
        experiment = experiments.filter(e => e.name == experiment)[0]
    }
    experiment.visible = !experiment.visible
    update()
}

/**
 * @function update_defs
 * @brief Updates the markers
 */
function update_defs() {
    let circles = d3.select('defs').selectAll('marker.circle')
        .data(experiments)
        .join('marker')
        .attr('id', e => `circle_${color(e.name).replace('#', '')}`)
        .attr('orient', 'auto')
        .attr('class', 'circle')
        .attr('markerWidth', connectorRadius)
        .attr('markerHeight', connectorRadius)
        .attr('markerUnits', 'userSpaceOnUse')
        .attr('overflow', 'visible')

    circles.append('circle')
        .attr('cx', 0)
        .attr('cy', 0)
        .attr('r', connectorRadius)
        .attr('fill', e => color(e.name))
        .attr('stroke', 'none')

    let s_arrows = d3.select('defs').selectAll('marker.s_arrow')
        .data(experiments)
        .join('marker')
        .attr('id', e => `s_arrow_${color(e.name).replace('#', '')}`)
        .attr('viewBox', '-0 -5 10 10')
        .attr('refX', 1)
        .attr('refY', 0)
        .attr('orient', 'auto')
        .attr('markerWidth', 15)
        .attr('markerHeight', 15)
        .attr('markerUnits', 'userSpaceOnUse')
        .attr('overflow', 'visible')
        .append('svg:path')
        .attr('d', 'M 0,0 L 7,-3 L 7,3')
        .attr('transform', 'scale(1.5)')
        .attr('fill', e => color(e.name))
        .style('stroke', 'none')

    let t_arrows = d3.select('defs').selectAll('marker.t_arrow')
        .data(experiments)
        .join('marker')
        .attr('id', e => `t_arrow_${color(e.name).replace('#', '')}`)
        .attr('viewBox', '-0 -5 10 10')
        .attr('refX', 10)
        .attr('refY', 0)
        .attr('orient', 'auto')
        .attr('markerWidth', 15)
        .attr('markerHeight', 15)
        .attr('overflow', 'visible')
        .attr('markerUnits', 'userSpaceOnUse')
        .append('svg:path')
        .attr('d', 'M 0,-3 L 7 ,0 L 0,3')
        .attr('transform', 'scale(1.5)')
        .attr('fill', e => color(e.name))
        .style('stroke', 'none')
}

/**
 * @function update_nodes
 * @brief Updates the nodes
 */
function update_nodes() {
    // Apply the general update pattern to the nodes
    let node = d3.select('#nodeGroup').selectAll('.node')
        .data(nodes.filter(n => n.visible), n => n.id)
        .join(
            enter => enter.append('g')
                .attr('id', n => `node-${n.id}`)
                .call(n => {

                    n.append('text')
                        .attr('font-family', 'Arial, Arial, Helvetica, sans-serif')
                        .style('font-size', '12px')
                        .attr('text-anchor', 'middle')
                        .attr('dominant-baseline', 'central')
                        .attr('x', 0)
                        .attr('y', 0)
                        .attr('fill', n => {
                            return n.variability === null ||
                            d3.select('#experiment1').property('value') == d3.select('#experiment2').property('value') ? '#333333' : '#ffffff'
                        })
                        .style('pointer-events', 'none')
                        .text(get_name)

                    n.insert('rect', 'text')
                        .attr('stroke', '#333333')
                        .attr('fill', get_color)
                        .attr('stroke-width', 2)
                        .attr('opacity', .8)
                        .style('cursor', 'pointer')
                        .attr('stroke-dasharray', n => n.is_ref ? 6 : 0)
                        .attr('width', n => {
                            let bbox = d3.select(`#node-${n.id}`).select('text').node().getBBox()
                            n.width = bbox.width+6
                            return n.width
                        })
                        .attr('height', n => {
                            let bbox = d3.select(`#node-${n.id}`).select('text').node().getBBox()
                            n.height = bbox.height+6
                            return n.height
                        })
                        .attr('transform', n => `translate(-${(n.width/2)},-${(n.height/2)})`)
                        .on('mouseover', n => {
                            if (d3.zoomTransform(svg.node()).k <= 0.4) {
                                show_tooltip(`${get_name(n)}`)
                            }
                        })
                        .on('mouseout', n => {
                            tooltip.html('')
                                .style('opacity', 0)
                        })

                    n.append('text').attr('class', 'lock')
                        .html(lock_icon)
                        .attr('title', 'Click to release')
                        .attr('alt', 'Click to release')
                        .attr('text-anchor', 'middle')
                        .attr('dominant-baseline', 'middle')
                        .style('cursor', 'pointer')
                        .attr('title', 'Click to unlock')
                        .attr('opacity', n => n.locked ? 1 : 0)
                        .attr('transform', n => `translate(-${n.width/2},-${n.height/2})`)
                        .on('click', n => {
                            n.fx = null
                            n.fy = null
                            n.locked = false
                            update_nodes()
                            d3.event.stopPropagation()
                        })
                    n.append('text').attr('class', 'significance')
                        .text(get_stars)
                        .attr('fill', '#000000')
                        .attr('text-anchor', 'middle')
                        .attr('font-weight', 'bold')
                        .attr('dominant-baseline', 'middle')
                        .style('cursor', 'help')
                        .style('font-size', '20px')
                        .attr('transform', n => `translate(${n.width/2},-${n.height/2-5})`)
                        .on('mouseover', n => {
                            let e = [
                                experiments.filter(e => e.e1)[0].name,
                                experiments.filter(e => e.e2)[0].name
                            ]
                            let es = e.sort()
                            let e1 = es[0]
                            let e2 = es[1]
                            if (e1 === e2) {
                                return ''
                            }
                            let p = n.variability.filter(v => v.e1 == e1 && v.e2 == e2)[0].p

                            show_tooltip(`p = ${p.toFixed(4)}`)

                        })
                        .on('mouseout', n => {
                            tooltip.html('').style('opacity', 0)
                        })
                }),
            update => update
                .attr('id', n => `node-${n.id}`)
                .call(n => {
                    n.select('text').text(get_name)
                        .attr('fill', n => {
                            return n.variability === null ||
                            d3.select('#experiment1').property('value') == d3.select('#experiment2').property('value') ? '#333333' : '#ffffff'
                        })

                    n.select('rect')
                        .attr('fill', get_color)
                        .attr('width', n => {
                            let bbox = d3.select(`#node-${n.id}`).select('text').node().getBBox()
                            n.width = bbox.width+6
                            return n.width
                        })
                        .attr('height', n => {
                            let bbox = d3.select(`#node-${n.id}`).select('text').node().getBBox()
                            n.height = bbox.height+6
                            return n.height
                        })
                        .attr('transform', n => `translate(-${(n.width/2)},-${(n.height/2)})`)

                    n.select('.lock')
                        .attr('opacity', n => n.locked ? 1 : 0)
                        .attr('transform', n => `translate(-${n.width/2},-${n.height/2})`)

                    n.select('.significance').text(get_stars)
                        .attr('transform', n => `translate(${n.width/2},-${n.height/2-5})`)

                })
        )
        .attr('id', n => `node-${n.id}`)
        .attr('class', 'node')
        .call(d3.drag().on('start', drag_start).on('drag', drag_active).on('end', drag_end))
        .on('contextmenu', n => {
            n.showpanel = false
            n.hidden = true
            update()
        })
        .on('click', n => {
            n.showpanel = true
            update_ui()
            d3.select(`#panel-${n.id}`).raise()
        })
}

/**
 * @function update_edges
 * @brief Updates the edges
 */
function update_edges() {
    // Apply the general update pattern to the edges
    let edge = d3.select('#edgeGroup').selectAll('.edge').data(edges.filter(e => e.visible), e => e.index)
        .join('line')
        .attr('id', e => `edge-${e.index}`)
        .attr('class', 'edge')

        .attr('stroke', e => e.experiment == 'Reference' ? referenceColor : color(e.experiment))
        .attr('stroke-dasharray', 0)
        .attr('marker-end', e => {
            if (e.experiment == 'Reference') {
                return `url(#circle_ref)`
            }
            if (config.miami_show_arrows) {
                let s_fc = fractional_contribution(e.source, e.experiment)
                let t_fc = fractional_contribution(e.target, e.experiment)
                if (t_fc < s_fc) {
                    return `url(#t_arrow_${color(e.experiment).replace('#', '')})`
                }
            }
            return `url(#circle_${color(e.experiment).replace('#', '')})`
        })
        .attr('marker-start', e => {
            if (e.experiment == 'Reference') {
                return `url(#circle_ref)`
            }
            if (config.miami_show_arrows) {
                let s_fc = fractional_contribution(e.source, e.experiment)
                let t_fc = fractional_contribution(e.target, e.experiment)
                if (s_fc < t_fc) {
                    return `url(#s_arrow_${color(e.experiment).replace('#', '')})`
                }
            }
            return `url(#circle_${color(e.experiment).replace('#', '')})`
        })
        .on('mouseover', e => {
            show_tooltip(`d = ${e.distance.toFixed(3)}`)
            tooltip.style('color', color(e.experiment))

        })
        .on('mouseout', e => {
            tooltip.transition().style('opacity', 0).style('color', '#000000')
        })

    edges.filter(e => e.visible).forEach(e => {
        let curEdges = []
        let edgeGap = 3
        edges.filter(x => x.visible && x.target === e.target && x.source === e.source).forEach(x => {
            curEdges.push(x.experiment)
        })
        let i = curEdges.indexOf(e.experiment)
        let edgeWidth = edgeGap * (curEdges.length - 1)
        e.offset = -edgeWidth/2 + edgeGap*i
    })

}

/**
 * @function is_target
 * @brief Checks if there is a target between two nodes
 *
 * @param      {Object}   node1  Node object
 * @param      {Object}   node2  Node object
 * @return     {Boolean}  True is there is a target, false otherwise
 */
function is_target(node1, node2) {
    let vs = get_trend(node1)
    let vt = get_trend(node2)
    let l = ['increase', 'decrease', 'equal', 'pattern']
    return l.includes(vs) && l.includes(vt) && vs != vt
}


/**
 * @function update_targets
 * @brief Updates the targets
 */
function update_targets() {
    // Update targets
    d3.selectAll('.edge').attr('stroke-dasharray', 0)

    targets.forEach(t => {
        delete t.from
        if (!config.miami_show_targets || !t.source.visible || !t.target.visible || !t.edges.some(e => e.visible)) {
            t.visible = false
        } else {
            t.visible = is_target(t.source, t.target)
            if (t.visible) {
                d3.selectAll('.edge').filter(e => t.source.id == e.source.id && t.target.id == e.target.id)
                    .attr('stroke-dasharray', 8)
            }
        }
    })

    targets.filter(t => config.miami_show_targets &&
                   config.miami_show_references &&
                   t.target.visible && t.source.visible &&
                   t.edges.some(e => e.distance <= config.miami_distance_cutoff) &&
                   t.edges.every(e => e.removed) &&
                   is_target(t.source, t.target)
    ).forEach(t => {
        let p = find_path(t.source, t.target, true, 'Reference')[0]
        targets.filter(x => x != t && p.indexOf(x.target.id) > -1 && p.indexOf(x.source.id)  > -1)
            .forEach(x => {
                x.visible = true
                x.from = t
            })
    })


    d3.select('#targetGroup').selectAll('.target').data(targets.filter(t => t.visible), t => t.id)
        .join('text').text('X').attr('class', 'target')
        .style('cursor', 'help')
        .style('text-anchor', 'middle')
        .attr('fill', t => {
            if (t.from) {
                return '#666666'
            } else {
                return '#990000'
            }
        })
        .attr('font-family', 'sans-serif')
        .style('font-size', '18px')
        .attr('font-weight', 'bold')
        .attr('dominant-baseline', 'middle')
        .attr('transform', t => `translate(${(t.target.x+t.source.x)/2},${(t.target.y+t.source.y)/2})`)
        .on('mouseover', t => {
            if(t.from) {
                show_tooltip(`Original target: ${t.from.target.name} vs. ${t.from.source.name}`)
            } else {
                show_tooltip(`${t.source.name} vs ${t.target.name}`)
            }
        })
        .on('mouseout', t => tooltip.text('').style('opacity', 0))
}

/**
 * @function drag_start
 * @brief Starts the dragging process
 *
 * @param      {Object}   node   Node object
 */
function drag_start(node) {
    if (!d3.event.active) {
        sim.alphaTarget(0.3).restart()
    }
    node.fx = node.x
    node.fy = node.y
}

/**
 * @function drag_active
 * @brief Performs the dragging process
 *
 * @param      {Object}   node   Node object
 */
function drag_active(node) {
    node.fx = d3.event.x
    node.fy = d3.event.y

    node.locked = true

    let x = d3.event.sourceEvent.clientX - Number(tooltip.style('width').slice(0,-2))/2
    let y = d3.event.sourceEvent.clientY - Number(tooltip.style('height').slice(0,-2))-10

    tooltip.style('left', `${x}px`)
        .style('top', `${y}px`)

    update_nodes()
}

/**
 * @function drag_end
 * @brief Ends the dragging process
 *
 * @param      {Object}   node   Node object
 */
function drag_end(node) {
    if (!d3.event.active) {
        sim.alphaTarget(0)
    }
    if (!node.locked){
        node.fx = null
        node.fy = null
    }
}

/**
 * @function get_name
 * @brief Returns the node name
 *
 * @param      {Object}   node   Node object
 * @return     {String}   The name based on selected options
 */
function get_name(node) {
    switch (parseInt(config.miami_node_name)) {
        case 2:
            return `${(parseFloat(node.rt)/1000/60).toFixed(2)} min`
        break
        case 3:
            return `${(parseFloat(node.rt)/1000).toFixed(2)} s`
        break
        case 1:
            return `RI ${node.ri.toFixed(2)}`
        break
        case 0:
            return `${node.name}`
        break
        default:
            return `???`
    }
}

/**
 * @function show_tooltip
 * @brief Shows the tooltip
 *
 * @param      {String}   text   The tooltip text
 */
function show_tooltip(text) {
    tooltip.html(text)
        .style('opacity', 1)
    let x = d3.event.pageX - Number(tooltip.style('width').slice(0,-2))/2
    let y = d3.event.pageY - Number(tooltip.style('height').slice(0,-2)) - 10
    tooltip.style('left', `${x}px`)
        .style('top', `${y}px`)
}

/**
 * @function get_variability
 * @brief Returns the variability value
 *
 * @param      {Object}   node   Node object
 * @return     {Float}   The variability
 */
function get_variability(node) {
    if(experiments.length < 2 || experiments.filter(e => e.e1 || e.e2).length < 2) return

    let e = [
        experiments.filter(e => e.e1)[0].name,
        experiments.filter(e => e.e2)[0].name
    ]
    let es = e.sort()
    let e1 = es[0]
    let e2 = es[1]

    if (e1 === e2 || node.variability === null) {
        return 0
    }

    return node.variability.filter(v => v.e1 == e1 && v.e2 == e2)[0].v
}

/**
 * @function get_trend
 * @brief Returns the trend based on variability
 *
 * @param      {Object}   node   Node object
 * @return     {String}   String representing the variability trend
 */
function get_trend(node) {
    if(experiments.length < 2 || experiments.filter(e => e.e1 || e.e2).length < 2) return

    let e = [
        experiments.filter(e => e.e1)[0].name,
        experiments.filter(e => e.e2)[0].name
    ]
    let es = e.sort()
    let e1 = es[0]
    let e2 = es[1]

    if (e1 === e2 || node.variability === null) {
        return 'none' // no variability
    }

    let v = node.variability.filter(v => v.e1 == e1 && v.e2 == e2)[0].v

    if (Math.abs(v) < config.miami_variability_cutoff) {
        return 'equal'
    }
    if (v < 0) {
        let m1 = node.mids.filter(m => m.experiment == e1)
        let m2 = node.mids.filter(m => m.experiment == e2)

        for (let i = 1; i < m1.length; i++) {
            if (m1[i].mean > 0 && m2[i].mean > 0 && m1[i].mean < m2[i].mean &&
                Math.abs(m1[i].mean - m2[i].mean) > config.miami_variability_cutoff*0.5) {
                return 'pattern'
            }
        }
        return 'decrease'
    } else if (v > 0) {
        return 'increase'
    } else {
        return 'x'
    }
}

/**
 * @function get_color
 * @brief Returns the color based on variability
 *
 * @param      {Object}   node   Node object
 * @return     {String}   Hex color string
 */
 function get_color(node) {

    let v = get_trend(node)

    switch (v) {
        case 'increase':
            return '#009900'
        break
        case 'decrease':
            return '#990000'
        break
        case 'equal':
            return '#666666'
        break
        case 'none':
            return '#eeeeee'
        break
        case 'pattern':
            return '#ffaa00'
        break
        default:
            return '#eeeeee'
    }
}

/**
 * @function get_stars
 * @brief Gets number of stars for significance
 *
 * @param      {Object}   node   Node object
 * @return     {String}   String with number of stars based on p-value
 */
function get_stars(node) {
    if (node.variability === null || experiments.filter(e => e.e1 || e.e2).length < 2) {
        return ''
    }
    let e = [
        experiments.filter(e => e.e1)[0].name,
        experiments.filter(e => e.e2)[0].name
    ]
    let es = e.sort()
    let e1 = es[0]
    let e2 = es[1]
    if (e1 === e2) {
        return ''
    }
    let p = node.variability.filter(v => v.e1 == e1 && v.e2 == e2)[0].p

    let stars = ''

    for (let x of pvalues) {
        if (p !== null && p < x) {
            stars += '*'
        }
    }
    return stars
}

/**
 * @function get_neighbours
 * @brief Returns all connected nodes for a given node and experiment
 *
 * @param      {Node}   node   The node object
 * @param      {string} experiment The experiment
 * @return     {Array}  The neighbouring nodes
 */
function get_neighbours(node, experiment=null) {
    var neighbours = []

    node.edges.filter(e => {
        if (experiment == null) {
            return e.visible
        } else {
            return e.visible && e.experiment == experiment
        }

    }).forEach(e => {
        neighbours.push(e.target == node ? e.source : e.target)
    })

  return neighbours
}

/**
 * @function get_intersection
 * @brief Calculates the intersection of a node and an edge
 *
 * @param      {Edge}  edge    The edge
 * @param      {Node}  node    The node
 * @return     {Array}   The x, y coordinates of the intersection.
 */
function get_intersection(x1, y1, x2, y2, node) {
  let left = node.x - node.width / 2
  let right = node.x + node.width / 2
  let top = node.y - node.height / 2
  let bottom = node.y + node.height / 2
  let offset = 0

  let x3, y3, x4, y4
  let t, u
  let points = []
  let arrNodes = [
    [left, top, left, bottom],     // left line
    [right, top, right, bottom],   // right line
    [left, top, right, top],       // top line
    [left, bottom, right, bottom]  // bottom line],
  ]

  for (let x of arrNodes) {
    x3 = x[0]
    y3 = x[1]
    x4 = x[2]
    y4 = x[3]

    t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) /
        ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) /
        ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))

    if (t >= 0.0 && t <= 1.0 && u >= 0.0 && u <= 1.0) {
      let px = x1 + t * (x2 - x1)
      let py = y3 + u * (y4 - y3)
      return [px, py]
      break
    }
  }

  return [node.x, node.y]
}

/**
 * @function import_variability
 * @brief Converts variability data
 *
 * @param      {Object}   data   Object with variability data
 * @return     {Array}    Array with variability objects
 */
function import_variability(data) {
  let tmp = []
  for (e1 in data.m0) {
    for (e2 in data.m0[e1]) {
      tmp.push({
        'e1': e1,
        'e2': e2,
        'v': data.m0[e1][e2][0],
        'p': data.m0[e1][e2][1]
      })
    }
  }
  return tmp
}

/**
 * @function import_mids
 * @brief Converts MID data
 *
 * @param      {Object}   data   Object with MID data
 * @return     {Array}    Array with MID objects
 */
function import_mids(data) {
  let tmp = []
  for (let exp in data) {
    for (let i in data[exp]){
      tmp.push(
        {
          'group': `M${i}`,
          'experiment': exp,
          'mean': data[exp][i][0],
          'err': data[exp][i][1]
        }
      )
    }
  }
  return tmp
}

/**
 * @function import_quant
 * @brief Converts quantification data
 *
 * @param      {Object}   data   Object with quantification data
 * @return     {Array}    Array with quantification objects
 */
function import_quant(data) {
    let tmp = []
    for (let exp in data) {
        tmp.push(
        {
            'group': exp,
            'experiment': '',
            'mean': data[exp][0],
            'err': data[exp][1]
        }
        )
    }
    return tmp
}

/**
 * @function possible_targets
 * @brief Gets all possible targets from a set of edges
 *
 * @return     {Array}    Array with target objects
 */
function possible_targets() {
    let targets = {}
    let i = 0
    edges.forEach(e => {
        if (!(`${e.source.id}-${e.target.id}` in targets)) {
            targets[`${e.source.id}-${e.target.id}`] = {
                id: i,
                source: e.source,
                target: e.target,
                visible: false,
                edges: []
            }
            i++
        }
        targets[`${e.source.id}-${e.target.id}`].edges.push(e)
    })

    return Object.values(targets)
}

/**
 * @function find_path
 * @brief Performs a breath-first search in a tree
 *
 * @param      {Node}    node_from   The node from
 * @param      {Node}   node_to     The node to
 * @param      {boolean}  shortest    If `true` only the shortest path will be returned
 * @param      {string}   experiment  The experiment to use for the path, `null` for all
 * @return     {Array}    Array of paths (array of node ids)
 */
function find_path(node_from, node_to, shortest = false, experiment = null) {
  if (node_from === undefined || node_to === undefined) {
    return [[]]
  }
  var paths = []
  var path = [node_from.id]
  var node = node_from
  var queue = [[node, path]]

  while (queue.length > 0) {
    var cur = queue.shift()
    node = cur[0]
    path = cur[1]

    for (var node of get_neighbours(node, experiment)) {
      if (!path.includes(node.id)) {
        var tmp = [].concat(path)
        tmp.push(node.id)
        if (node.id == node_to.id) {
          paths.push(tmp)
          if (shortest) {
            return paths
          }
        } else {
          queue.push([node, tmp])
        }
      }
    }
  }
  if (paths.length == 0) {
    paths = [[]]
  }
  return paths
}

/**
 * @function get_online_json
 * @brief Loads a json file from the web
 *
 * @param      {string}  url    The url to load from
 */
function get_online_json(url) {
    filename = url.split('/')[url.split('/').length-1]
    d3.json(url)
        .then((json) => {
            load_json(json)
        })
        .catch((err) => {
            console.log(err)
        })
}

/**
 * @function get_local_json
 * @brief Loads a local json file using the file api
 *
 * @param      {string}  file    The local file to load
 */
function get_local_json(file) {
    if (!window.FileReader) {
        console.log('FileReader is not supported :(')
        return
    }

    // Check if json format
    if (!file.type.match('application/json')) {
        console.log('Only json format is supported.')
        return
    }

    let reader = new FileReader()

    reader.onload = (f => {
        filename = f.name
        return e => {
          load_json(e.target.result)
        }
    })(file)
    reader.readAsText(file)
}

/**
 * @function get_config_value
 * @brief Returns one value from config
 *
 * @returns      {Various}   The corresponding config value
 */
function get_config_value(key) {
    return config[key]
}


/**
 * @function load_json
 * @brief Loads the data from a JSON file and inits the graph/ui
 *
 * @param      {JSON/String}   json  The data in JSON format or as string
 */
function load_json(json) {
    if (typeof json == 'string') {
        data = JSON.parse(json)
    } else {
        data = json
    }

    d3.select('title').html(`MIAMI - ${filename}`)

    config = data.config
    experiments = data.experiments
    nodes = data.nodes
    edges = data.edges

    config.miami_node_name = config.miami_node_name ? config.miami_node_name : name
    config.miami_distance_cutoff = config.miami_distance_cutoff ? config.miami_distance_cutoff : distanceCutoff
    config.miami_variability_cutoff = config.miami_variability_cutoff ? config.miami_variability_cutoff : variabilityCutoff
    config.miami_quant_cutoff = config.miami_quant_cutoff ? config.miami_quant_cutoff : quantCutoff
    config.miami_nodes_over_edges = config.miami_nodes_over_edges ? config.miami_nodes_over_edges : nodesOverEdges
    config.miami_single_nodes = config.miami_single_nodes ? config.miami_single_nodes : showSingleNodes
    config.miami_show_arrows = config.miami_show_arrows ? config.miami_show_arrows : showArrows
    config.miami_show_targets = config.miami_show_targets ? config.miami_show_targets : showTargets
    config.miami_show_references = config.miami_show_references ? config.miami_show_references : showReferences
    config.miami_remove_edges = config.miami_remove_edges ? config.miami_remove_edges : removeEdges

    if (WEBVERSION) {
        d3.select('#distanceSlider').node().value = config.miami_distance_cutoff
        d3.select('#distanceLabel').html(parseFloat(d3.select('#distanceSlider').node().value).toFixed(2))

        d3.select('#variabilitySlider').node().value = config.miami_variability_cutoff
        d3.select('#variabilityLabel').html(parseFloat(d3.select('#variabilitySlider').node().value).toFixed(2))

        d3.select('#quantSlider').node().value = config.miami_quant_cutoff
        d3.select('#quantLabel').html(parseInt(d3.select('#quantSlider').node().value))

        d3.select('#nodeName').node().value = config.miami_node_name
        d3.select('#nodesOverEdges').node().checked = config.miami_nodes_over_edges
        d3.select('#showSingleNodes').node().checked = config.miami_single_nodes
        d3.select('#showArrows').node().checked = config.miami_show_arrows
        d3.select('#showTargets').node().checked = config.miami_show_targets
        d3.select('#showReferences').node().checked = config.miami_show_references
        d3.select('#removeEdges').node().checked = config.miami_remove_edges
    }

    color = typeof cols === 'undefined' ?  d3.scaleOrdinal(d3.schemeCategory10) : d3.scaleOrdinal().domain([0, 8]).range(cols)

    sim = d3.forceSimulation(nodes)
        .force('charge', d3.forceManyBody().strength(-300))
        .force('collide', d3.bboxCollide((node, i) => [
            [-node.width / 2 - nodeGap, -node.height / 2 - nodeGap],
            [node.width / 2 + nodeGap, node.height / 2 + nodeGap]
        ]))
        .force('link', d3.forceLink(edges).id(e => e.id).distance(200))
        .force('forceX', d3.forceX(0).strength(0.05))
        .force('forceY', d3.forceY(0).strength(0.05))

    let checkE1 = false
    let checkE2 = false
    experiments.forEach(e => {
        e.visible = true
        if (!checkE1 && e.e1) checkE1 = true
        if (!checkE2 && e.e2) checkE1 = true
    })

    if (!checkE1 && experiments.length >= 1){
        experiments[0].e1 = true
    }
    if (!checkE1 && experiments.length >= 2) {
        experiments[1].e2 = true
    }

    nodes.forEach(n => {
        n.edges = []
        n.quant = Array.isArray(n.quant) ? n.quant : import_quant(n.quant)
        n.mids = Array.isArray(n.mids) ? n.mids : import_mids(n.mids)
        if (n.variability !== null) {
            n.variability = n.variability.m0 === undefined ? n.variability : import_variability(n.variability)
        }

    })

    edges.forEach(e => {
        e.source.edges.push(e)
        e.target.edges.push(e)
    })

    targets = possible_targets()

    update()

    sim.on('tick', () => {
        let e = d3.selectAll('.edge')
            .each(e => {
                let s = e.source
                let t = e.target

                let len = Math.sqrt((s.x-t.x)*(s.x-t.x)+(s.y-t.y)*(s.y-t.y))
                let tx = t.x + e.offset * (t.y-s.y) / len
                let ty = t.y + e.offset * (s.x-t.x) / len
                let sx = s.x + e.offset * (t.y-s.y) / len
                let sy = s.y + e.offset * (s.x-t.x) / len

                let sp = get_intersection(tx, ty, sx, sy, e.source)
                let tp = get_intersection(tx, ty, sx, sy, e.target)

                e.x1 = sp[0]
                e.y1 = sp[1]
                e.x2 = tp[0]
                e.y2 = tp[1]
            })

        e.attr('x1', e => e.x1)
            .attr('y1', e => e.y1)
            .attr('x2', e => e.x2)
            .attr('y2', e => e.y2)

        d3.selectAll('.node')
            .attr('transform', n => `translate(${n.x},${n.y})`)
        d3.selectAll('.target')
            .attr('transform', t => `translate(${(t.target.x+t.source.x)/2},${(t.target.y+t.source.y)/2})`)
    })
}

/**
 * @function clear_json
 * @brief Clears the data and resets the ui
 */
function clear_json() {

    data = {}
    config = {}
    experiments = []
    nodes = []
    edges = []

    filename = null

    d3.select('title').html(`MIAMI`)

    update_ui()
    update_nodes()
    update_edges()
    update_targets()
}

/**
 * @function select_experiments
 * @brief Updates the selected experiments
 *
 * @param      {String}   e1  Experiment 1
 * @param      {String}   e2  Experiment 2
 */
function select_experiments(e1, e2) {
    experiments.forEach(e => {
        e.e1 = e.name == e1
        e.e2 = e.name == e2
    })

    update_nodes()
    update_ui()
}

/**
 * @function move2pos
 * @brief Moves the viewport to a given point
 *
 * @param      {number}   x          The x coordinate
 * @param      {number}   y          The y coordinate
 */
function move2pos(x, y) {
    x = x + width / 2
    y = y + height / 2
    svg.call(zoom.transform, () => d3.zoomIdentity.translate(x, y))
}


/**
 * @function move2target
 * @brief Simulates a click on a node.
 *
 * @param      {Integer}   node_idx  The node index
 *
 */
function move2target(target_idx) {
    let t = targets.filter(t => t.visible)[target_idx]

    console.log(t.source.x, t.target.x, (t.source.x + t.target.x)/2)
    console.log(t.source.y, t.target.y, (t.source.y + t.target.y)/2)

    let x = (t.source.x + t.target.x)/2
    let y = (t.source.y + t.target.y)/2

    move2pos(-x, -y)
}

/**
 * @function node_click
 * @brief Simulates a click on a node.
 *
 * @param      {Integer}   node_idx  The node index
 *
 */
function node_click(node_idx) {
  let n = nodes.filter(n => n.visible && !n.hidden)[node_idx]

  var event = document.createEvent('MouseEvents')
  event.initMouseEvent('click')
  d3.select(`#node-${n.id}`).node().dispatchEvent(event)
}


/**
 * @function get_nodes
 * @brief Returns all valid node data
 *
 * @return     {Array}  Array with Arrays with index, id, name,
 *                      ion, isRef, RT and RI as value
 */
function get_nodes() {
  let output = []
  let i = 0
  nodes.forEach(n => {
    if (n.visible && !n.hidden) {
        output.push(
            [i, n.id, n.name, n.ion, n.is_ref, n.rt, n.ri])
        i++
    }
  })
  return output
}

/**
 * @function get_targets
 * @brief Returns an array of targets
 *
 * @returns    {Array} Array of targets containing edge id and
 *                     names of nodes.
 */
function get_targets() {
  if (typeof targets === 'undefined') {
    return []
  }

  let output = []
  let n = 0
  targets.filter(t => t.visible).forEach(edge => {
    output.push([n, edge.source.name, edge.target.name])
    n++
  })
  return output
}

/**
 * @function get_edges
 * @brief Returns all valid edge ids
 *
 * @return     {Array}  Array with Arrays with edge ids
 */
function get_edges() {
  let output = []
  edges.forEach(e => {
    if (e.visible) {
        output.push([e.index])
    }
  })

  return output
}

/**
 * @function get_experiments
 * @brief Gets the colors for all experiments.
 *
 * @returns    {Array} Array with experiment and color as array
 */
function get_experiments() {
  let output = []
  for (exp of data.experiments) {
    output.push([exp.name, color(exp.name)])
  }
  return output
}

/**
 * @function fractional_contribution
 * @brief Calculates the Fractional contribution of an array of MID values
 *
 * @param      {Node}   node  The node object
 * @param      {String} experiment  The name of the experiment
 * @return     {Float}  The fractional contribution
 */
function fractional_contribution(node, experiment) {
  let fc = 0
  let mids = node.mids.filter(m => m.experiment == experiment)
  let i = 0
  for (let m of mids) {
    fc += m.mean * i
    i++
  }
  return fc / (i - 1)
}

/**
 * @function check_data
 * @brief Checks if visualization data is valid
 *
 * @returns    {bool} True if data is present, false otherwise
 */
function check_data() {
    return data !== undefined && nodes !== undefined && edges !== undefined
}

/**
 * @function mids2svg
 * @brief Converts data to svg plot
 *
 * @returns    {String} SVG barplot as string
 */
function mids2svg(node_idx) {
    let data = nodes.filter(n => n.visible && !n.hidden)[node_idx]
    let plot = barplot(data.mids, data.variability)
    return plot.outerHTML
}

/**
 * @function data2string
 * @brief Returns the JSON data as string
 *
 * @returns    {String} JSON data as string
 */
function data2string() {
    data.edges.forEach(e => {
        e.source = e.source.id
        e.target = e.target.id
    })
    data.nodes.forEach(n => {
        delete n.edges
    })
    let payload = JSON.stringify(data, null, ' ')
    load_json(data)
    return payload
}

/**
 * @function barplot
 * @brief Creates a SVG barplot
 *
 * @param   {Object}    data            The node object
 * @param   {Object}    variability     The name of the experiment
 * @param   {Integer}   width           The barplot width
 * @param   {Integer}   height          The barplot height
 * @return  {Object}    The SVG as D3 node
 */
function barplot(data, variability=null, width=400, height=300) {
  let mt = variability === null ? 10 : 10+16*variability.length
  let margin = {left: 100, right: 20, top: mt, bottom: 40}
  let iwidth = width - margin.left - margin.right
  let iheight = height - margin.top - margin.bottom
  let max = Math.max(...data.map(d => d.mean + d.err))

  let x_groups = d3.scaleBand().range([0, iwidth]).padding(0.05)
  let x_experiments = d3.scaleBand().padding(0.05)
  let y = d3.scaleLinear().range([0, iheight]).domain([max < 1 ? 1 : max, 0])


  let svg = d3.create('svg')
    // .style('background-color', '#ffffff')
    .attr('width', width)
    .attr('height', height)

  let group = svg.append('g').style('font-size', `12px`).style('text-anchor', 'middle')
    .attr('font-family', 'sans-serif').attr('dominant-baseline', 'middle')
    .attr('transform', `translate(${margin.left},${margin.top})`)

  let experiments = Array.from(new Set(data.map(d => d.experiment)))
  let groups = Array.from(new Set(data.map(d => d.group)))

  x_groups.domain(groups)
  x_experiments.domain(experiments).rangeRound([0, x_groups.bandwidth()])

  // Bars
  group.selectAll('rect')
      .data(data).join('rect')
      .attr('transform',d => `translate(${x_groups(d.group)},0)`)
      .attr('x', d => x_experiments(d.experiment))
      .attr('width', x_experiments.bandwidth())
      .attr('fill', d => color(d.experiment === '' ? d.group : d.experiment))
      .attr('y', d => d.mean > 0 ? y(d.mean) : y(0))
      .attr('height', d => Math.abs(y(d.mean)-y(0)))
  // Error bars
  group.selectAll('line')
      .data(data).join('line')
      .attr('transform', d => `translate(${x_groups(d.group)},0)`)
      .attr('stroke', '#333333')
      .attr('x1', d => x_experiments(d.experiment) + x_experiments.bandwidth() / 2)
      .attr('x2', d => x_experiments(d.experiment) + x_experiments.bandwidth() / 2)
      .attr('y1', d => y(d.mean + d.err))
      .attr('y2', d => y(d.mean - d.err))
  // pvalue
  if (variability !== null) {
    let dy = 0
    let pval = group.selectAll('.pval').data(variability)
      .join('g').attr('class', '.pval').attr('transform', d => `translate(0,-${16*dy++})`)

    pval.append('path')
      .attr('stroke', '#000000')
      .attr('fill', 'none')
      .attr('transform',`translate(0,${y(max)-3})`)
      .attr('d', d =>
      `M${x_experiments(d.e1) + x_experiments.bandwidth() / 2},0`
      +`V-3`
      +`H${x_experiments(d.e1) + x_experiments(d.e2) + x_experiments.bandwidth() / 2}`
      +`V0`
      )
    pval.append('text')
      .style('font-size', d => d.p < 0.05 ? `16px` : `12px`)
      .text(d => d.p < 0.005 ? '**' : d.p < 0.05 ? '*' : 'ns')
      .attr('text-anchor', 'middle')
      .attr('x', d => x_experiments(d.e1)+x_experiments(d.e2)/2+x_experiments.bandwidth()/2)
      .attr('y', d => d.p < 0.05 ? 0 : -8)
      .attr('transform',`translate(0,${y(max)-3})`)

  }
  // Axes
  group.append('g').attr('transform', `translate(0,${iheight})`)
      .call(d3.axisBottom(x_groups))

  let y_axis = group.append('g').call(d3.axisLeft(y))

  group.append('text').attr('x', -iheight/2).attr('y', -margin.left/1.5)
      .text('Abundance').attr('transform', 'rotate(-90)')

  // Legend
  if (experiments.every(d => d !== '')) {
    var legend = svg.selectAll('.legend')
      .data(experiments).join('g')
      .style('font-size', `9px`)
      .attr('text-anchor', 'end')
      .attr('font-family', 'sans-serif')
      .attr('transform', (d, i) => `translate(${iwidth+margin.left-10},${(margin.top+i*15+5)})`)
    legend.append('rect').attr('width', 10).attr('height', 10)
      .attr('fill', d => color(d))
    legend.append('text').attr('transform', 'translate(-5,8)').text(d => d)
  }
  return svg.node()
}

// Ui elements only in web version
if (WEBVERSION) {
    d3.select('#ui').style('display', 'block')
    // Load file listener
    d3.select('#loadfile').on('change', (e) => {
        get_local_json(d3.event.target.files[0])
    })

    d3.select('#distanceSlider').on('input', () => {
        let value = d3.select('#distanceSlider').node().value
        d3.select('#distanceLabel').html(parseFloat(value).toFixed(2))
        config.miami_distance_cutoff = parseFloat(value)
        update()
    })
    d3.select('#variabilitySlider').on('input', () => {
        let value = d3.select('#variabilitySlider').node().value
        d3.select('#variabilityLabel').html(parseFloat(value).toFixed(2))
        config.miami_variability_cutoff = parseFloat(value)
        update(false)
    })
    d3.select('#quantSlider').on('input', () => {
        let value = d3.select('#quantSlider').node().value
        d3.select('#quantLabel').html(parseInt(value))
        config.miami_quant_cutoff = parseInt(value)
        update(true)
    })
    d3.select('#nodeName').on('change', () => {
        config.miami_node_name = d3.select('#nodeName').node().value
        update()
    })
    d3.select('#nodesOverEdges').on('change', function() {
        config.miami_nodes_over_edges = d3.select('#nodesOverEdges').node().checked
        update()
    })

    d3.select('#showSingleNodes').on('change', function() {
        config.miami_single_nodes = d3.select('#showSingleNodes').node().checked
        update()
    })
    d3.select('#showArrows').on('change', function() {
        config.miami_show_arrows = d3.select('#showArrows').node().checked
        update()
    })
    d3.select('#showTargets').on('change', function() {
        config.miami_show_targets = d3.select('#showTargets').node().checked
        update(false)
    })
    d3.select('#showReferences').on('change', function() {
        config.miami_show_references = d3.select('#showReferences').node().checked
        update()
    })
    d3.select('#removeEdges').on('change', function() {
        config.miami_remove_edges = d3.select('#removeEdges').node().checked
        update()
    })

    // X buttons
    d3.selectAll('.close').on('click', () => {
        let panel = d3.select(d3.event.target.parentElement)
        panel.style('display', 'none')

        let cb = d3.select(`#cb_${panel.attr('id')}`)
        if (cb.size() > 0) {
            cb.node().checked = false
        }

    })

    // The menu will be always on top
    d3.select('#menu').raise()

    // Panel checkboxes
    d3.select('#options').style('display', () => d3.select('#cb_options').node().checked ? 'block' : 'none')
    d3.select('#metabolites').style('display', () => d3.select('#cb_metabolites').node().checked ? 'block' : 'none')
    d3.select('#cb_metabolites').on('click', () => {
        d3.select('#panel_metabolites').dispatch('click')
    })
    d3.select('#panel_metabolites').on('click', () => {
        let cb = d3.select('#cb_metabolites').node()
        cb.checked = !cb.checked
        d3.select('#metabolites').style('display', () => cb.checked ? 'block' : 'none')
    })
    d3.select('#cb_options').on('click', () => {
        d3.select('#panel_options').dispatch('click')
    })
    d3.select('#panel_options').on('click', () => {
        let cb = d3.select('#cb_options').node()
        cb.checked = !cb.checked
        d3.select('#options').style('display', () => cb.checked ? 'block' : 'none')
    })

    // Load JSON
    d3.select('#file_load').on('click', () => {
        document.getElementById('loadfile').click()
    })

    // Save JSON
    d3.select('#file_save').on('click', () => {
        data.edges.forEach(e => {
            e.source = e.source.id
            e.target = e.target.id
        })
        data.nodes.forEach(n => {
            delete n.edges
        })

        d3.select('#file_save')
            .attr('href', `data:application/octet-stream;base64,${btoa(JSON.stringify(data, null, ' '))}`)
            .attr('download', filename)

        load_json(data)
    })

    // Close JSON
    d3.select('#file_close').on('click', clear_json)

    // Export the graph
    d3.select('#export_graph').on('click', () => {
        let copy = d3.select('svg#graph').clone(true)
        copy.selectAll('.lock').remove()
        copy.selectAll('.node').select('text')
            .attr('transform', 'translate(0, 5)')
        let payload = btoa(copy.node().outerHTML)
        copy.remove()

        d3.select('#export_graph')
            .attr('href', `data:application/octet-stream;base64,${payload}`)
            .attr('download', filename.replace('.json', '_network.svg'))
    })
    // Export MIDs as csv
    d3.select('#export_mids').on('click', () => {
        let payload = mids2csv()
        d3.select('#export_mids')
            .attr('href', `data:application/octet-stream;base64,${btoa(payload)}`)
            .attr('download', filename.replace('.json', '_mids.csv'))

    })
    // Export targets as csv
    d3.select('#export_targets').on('click', () => {

    })
    // Open the info panel
    d3.select('#about_info').on('click', () => {
        d3.select('#about').style('display', 'block')
    })
    // Open the help panel
    d3.select('#about_help').on('click', () => {
        d3.select('#help').style('display', 'block')
    })
}

/**
 * @function targets2csv
 * @brief Converts all current targets to csv format
 *
 * @return     {String}  The targets in csv format
 */
function targets2csv() {
    let exp1 = experiments.filter(e => e.e1)[0]
    let exp2 = experiments.filter(e => e.e2)[0]

    let output = `Experiment1;${exp1.name};Experiment2;${exp2.name}\n` +
                 `Variability threshold:;${variabilityCutoff}\n\n`

    output += 'Metabolite1;RI1;RT1;Trend1;Variability1;' +
              ';Metabolite2;RI2;RT2;Trend2;Variability2;Reference?\n'

    targets.filter(target => target.visible).forEach(target => {
        let s = target.source
        let t = target.target
        output += `${s.name};${s.ri};${s.rt/1000/60};${get_trend(s)};${get_variability(s)};`
        output += `${t.name};${t.ri};${t.rt/1000/60};${get_trend(t)};${get_variability(t)};`
        output += `\n`;
    })

    return output
}

/**
 * @function mids2csv
 * @brief Converts MID data to csv string.
 *
 * @param      {Integer}   node_idx  The node index
 *
 * @returns    {String} Nodes in csv format
 */
function mids2csv(node_idx=null) {
    let mid_nodes
    if (node_idx === null) {
        mid_nodes = nodes
    } else {
        mid_nodes = [nodes.filter(n => n.visible && !n.hidden)[node_idx]]
    }

    let payload = ''
    let max = 0
    let header = `metabolite,ion,rt_min,ri,experiment`
    mid_nodes.forEach(n => {
        let met = `"${n.name}",${n.ion},${n.rt/1000/60},${n.ri}`
        let mids = {}
        n.mids.forEach(m => {
            if (!(m.experiment in mids)) {
                mids[m.experiment] = []
            }
            mids[m.experiment].push(m.mean)
            mids[m.experiment].push(m.err)
        })
        for (let m in mids) {
            max = Math.max(max, mids[m].length/2)
            payload += `${met},${m},${mids[m].join(',')}\n`
        }
    })
    for (let i=0; i<max; i++) {
        header += `,mean(M${i}),err(M${i})`
    }
    header += '\n'

    return `${header}${payload}`
}

function replacer() {
    const seen = new WeakSet
    return (key, value) => {
        if (typeof value === 'object' && value !== null) {
          if (seen.has(value)) return
          seen.add(value)
        }
        return value
    }
}

function run_code(code) {
  let res = eval(code)
  if (typeof res === 'object') {
    console.log(JSON.stringify(res, replacer()))
  } else {
    console.log(res)
  }
}
