# Changelog

## [1.1.1] - 2020-09-03

### Fixed

- Show references checkbox was not working

## [1.1.0] - 2020-02-29
### Added

- Changelog file
- Function is_target() checks if there is a target between two nodes

### Changed

- Targets on removed edges are now moved to the corresponding reference edges (as a gray X)

### Fixed

- Nodes with 'special patterns' (yellow) are now considered for target detection
- Experiments are now selected on load as saved in the json file
- Node text color is now correct when same experiment is selected

## [1.0.1] - 2019-09-13
### Added

- Missing about menu
- Missing menu css

### Fixed

- Node names now changing properly
- Menu entries are now properly clickable

## [1.0.0] - 2019-07-16
Initial release